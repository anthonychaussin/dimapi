<?php

namespace App\DataFixtures;

use App\Entity\Media;
use App\Entity\Oeuvre;
use App\Entity\Type;
use App\Entity\User;
use App\Entity\Visite;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DIMFixtures extends BaseFixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $this->loadData($manager);

        /** @var Oeuvre[] $type */
        $oeuvre = [];
        /** @var Media[] $type */
        $media = [];
        /** @var User[] $type */
        $user = [];
        /** @var Visite[] $type */
        $visite = [];
        /** @var Type[] $type */
        $type = [];

        $video = ['https://www.youtube.com/watch?v=PIzDM5JXl9Q', 'https://www.youtube.com/watch?v=SPg7hOxFItI', 'https://www.youtube.com/watch?v=yuOsQEjqEP0', 'https://www.youtube.com/watch?v=gY3p2e1-kN4'];

        array_push($type, ((new Type())->setLibelle('TEXT')));
        array_push($type, ((new Type())->setLibelle('PICTURE')));
        array_push($type, ((new Type())->setLibelle('VIDEO')));
        array_push($type, ((new Type())->setLibelle('HTML')));

        for ($i = 50; $i > 0; --$i) {
            array_push($oeuvre, (
            (new Oeuvre())
                ->setTitle($this->faker->text(50))));
        }

        for ($i = 1500; $i > 0; --$i) {
            $mediaTemp = (new Media())
                ->setIsDefault($this->faker->boolean(15))
                ->setOeuvre($oeuvre[$this->faker->randomKey($oeuvre)])
                ->setType($type[$this->faker->randomKey($type)]);

            switch ($mediaTemp->getType()->getLibelle()) {
                case 'TEXT':
                    $mediaTemp->setValue($this->faker->text());
                    break;
                case 'PICTURE':
                    $mediaTemp->setValue($this->faker->imageUrl());
                    break;
                case 'VIDEO':
                    $mediaTemp->setValue($video[$this->faker->randomKey($video)]);
                    break;
                case 'HTML':
                    $mediaTemp->setValue($this->faker->randomHtml());
                    break;
            }
            array_push($media, ($mediaTemp));
        }

        for ($i = 300; $i > 0; --$i) {
            $userTemp = (new User());
            $userTemp->setRoles(['ROLE_USER'])
                ->setUsername($this->faker->userName)
                ->setPassword($this->encoder->encodePassword($userTemp, $this->faker->password()));
            array_push($user, ($userTemp));
        }

        for ($i = 2500; $i > 0; --$i) {
            array_push($visite, (
            (new Visite())
                ->setOeuvre($oeuvre[$this->faker->randomKey($oeuvre)])
                ->setUser($user[$this->faker->randomKey($user)])
                ->setIsFromQrCode($this->faker->boolean(75))
                ->setMedia($media[$this->faker->randomKey($media)])
                ->setTimeStart($this->faker->dateTime())
            ));
        }
        foreach ($type as $t) {
            $manager->persist($t);
        }
        foreach ($oeuvre as $o) {
            $manager->persist($o);
        }
        foreach ($media as $m) {
            $manager->persist($m);
        }
        foreach ($user as $u) {
            $manager->persist($u);
        }
        foreach ($visite as $v) {
            $manager->persist($v);
        }
        $manager->flush();
    }
}
