<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200425181124 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE media (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, oeuvre_id INTEGER NOT NULL, type_id INTEGER NOT NULL, is_default BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX IDX_6A2CA10C88194DE8 ON media (oeuvre_id)');
        $this->addSql('CREATE INDEX IDX_6A2CA10CC54C8C93 ON media (type_id)');
        $this->addSql('CREATE TABLE oeuvre (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, title VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE type (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, libelle VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE TABLE user (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, username VARCHAR(180) NOT NULL, roles CLOB NOT NULL --(DC2Type:json)
        , password VARCHAR(255) NOT NULL)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649F85E0677 ON user (username)');
        $this->addSql('CREATE TABLE visite (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, oeuvre_id INTEGER DEFAULT NULL, media_id INTEGER DEFAULT NULL, user_id INTEGER NOT NULL, time_start DATETIME NOT NULL, is_from_qr_code BOOLEAN NOT NULL)');
        $this->addSql('CREATE INDEX IDX_B09C8CBB88194DE8 ON visite (oeuvre_id)');
        $this->addSql('CREATE INDEX IDX_B09C8CBBEA9FDD75 ON visite (media_id)');
        $this->addSql('CREATE INDEX IDX_B09C8CBBA76ED395 ON visite (user_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'sqlite', 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE media');
        $this->addSql('DROP TABLE oeuvre');
        $this->addSql('DROP TABLE type');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE visite');
    }
}
