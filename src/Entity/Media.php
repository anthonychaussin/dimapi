<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\MediaRepository")
 */
class Media
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsDefault;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Oeuvre", inversedBy="media")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Oeuvre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Type", inversedBy="media")
     * @ORM\JoinColumn(nullable=false)
     */
    private $Type;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Visite", mappedBy="Media")
     */
    private $visites;

    /**
     * @ORM\Column(type="string", length=1000000000)
     */
    private $Value;

    public function __construct()
    {
        $this->visites = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsDefault(): ?bool
    {
        return $this->IsDefault;
    }

    public function setIsDefault(bool $IsDefault): self
    {
        $this->IsDefault = $IsDefault;

        return $this;
    }

    public function getOeuvre(): ?Oeuvre
    {
        return $this->Oeuvre;
    }

    public function setOeuvre(?Oeuvre $Oeuvre): self
    {
        $this->Oeuvre = $Oeuvre;

        return $this;
    }

    public function getType(): ?Type
    {
        return $this->Type;
    }

    public function setType(?Type $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    /**
     * @return Collection|Visite[]
     */
    public function getVisites(): Collection
    {
        return $this->visites;
    }

    public function addVisite(Visite $visite): self
    {
        if (!$this->visites->contains($visite)) {
            $this->visites[] = $visite;
            $visite->setMedia($this);
        }

        return $this;
    }

    public function removeVisite(Visite $visite): self
    {
        if ($this->visites->contains($visite)) {
            $this->visites->removeElement($visite);
            // set the owning side to null (unless already changed)
            if ($visite->getMedia() === $this) {
                $visite->setMedia(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->getId() . '';
    }

    public function getValue(): ?string
    {
        return $this->Value;
    }

    public function setValue(string $Value): self
    {
        $this->Value = $Value;

        return $this;
    }
}
