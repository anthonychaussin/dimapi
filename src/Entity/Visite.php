<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ApiResource
 * @ORM\Entity(repositoryClass="App\Repository\VisiteRepository")
 */
class Visite
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $timeStart;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Oeuvre", inversedBy="visites")
     */
    private $Oeuvre;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Media", inversedBy="visites")
     */
    private $Media;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="visites")
     * @ORM\JoinColumn(nullable=false)
     */
    private $User;

    /**
     * @ORM\Column(type="boolean")
     */
    private $IsFromQrCode;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTimeStart(): ?DateTimeInterface
    {
        return $this->timeStart;
    }

    public function setTimeStart(DateTimeInterface $timeStart): self
    {
        $this->timeStart = $timeStart;

        return $this;
    }

    public function getOeuvre(): ?Oeuvre
    {
        return $this->Oeuvre;
    }

    public function setOeuvre(?Oeuvre $Oeuvre): self
    {
        $this->Oeuvre = $Oeuvre;

        return $this;
    }

    public function getMedia(): ?Media
    {
        return $this->Media;
    }

    public function setMedia(?Media $Media): self
    {
        $this->Media = $Media;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->User;
    }

    public function setUser(?User $User): self
    {
        $this->User = $User;

        return $this;
    }

    public function getIsFromQrCode(): ?bool
    {
        return $this->IsFromQrCode;
    }

    public function setIsFromQrCode(bool $IsFromQrCode): self
    {
        $this->IsFromQrCode = $IsFromQrCode;

        return $this;
    }

    public function __toString()
    {
        return $this->getId().'';
    }
}
